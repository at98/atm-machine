package com.zinkworks.atmmachine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zinkworks.atmmachine.controller.dto.AccountBalanceDTO;
import com.zinkworks.atmmachine.controller.dto.TransactionDTO;
import com.zinkworks.atmmachine.entity.ATM;
import com.zinkworks.atmmachine.entity.UserAccount;
import com.zinkworks.atmmachine.notes.WithdrawalRequest;
import com.zinkworks.atmmachine.service.ATMService;

/**
 * 
 * @author Arun.Singh
 *
 */
@RestController
@RequestMapping("/atm")
public class ATMController {

	private final ATMService atmService;

	@Autowired
	public ATMController(final ATMService atmService) {
		this.atmService = atmService;
	}

	@PostMapping
	public ResponseEntity<ATM> initializeAmountinATM(@RequestBody final ATM atmDetails) {
		return new ResponseEntity<ATM>(atmService.initializeAmountinATM(atmDetails), HttpStatus.CREATED);
	}

	@GetMapping("/getBalance/accountId/{accountId}/pin/{pin}")
	public ResponseEntity<AccountBalanceDTO> getAccountBalance(@PathVariable("accountId") final Long accountId,
			@PathVariable("pin") final int pin) {
		return new ResponseEntity<AccountBalanceDTO>(atmService.getAccountBalance(accountId, pin), HttpStatus.OK);
	}

	@PostMapping("/withdraw")
	public ResponseEntity<TransactionDTO> withdrawAmount(@RequestBody final WithdrawalRequest withdrawalRequest) {
		return new ResponseEntity<TransactionDTO>(atmService.withdrawAmount(withdrawalRequest), HttpStatus.OK);
	}

	@GetMapping("find/{id}")
	public ResponseEntity<ATM> findATM(@PathVariable final Long id) {
		return new ResponseEntity<ATM>(atmService.findATM(id), HttpStatus.OK);
	}

	@PostMapping("/addUser")
	public ResponseEntity<UserAccount> addAccountDetails(@RequestBody final UserAccount newAccount) {
		return new ResponseEntity<UserAccount>(atmService.addAccountDetails(newAccount), HttpStatus.CREATED);
	}
}
