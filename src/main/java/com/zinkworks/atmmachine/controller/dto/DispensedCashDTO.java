package com.zinkworks.atmmachine.controller.dto;

/**
 * 
 * @author Arun.Singh
 *
 */

public class DispensedCashDTO {

	private Integer note5 = Integer.valueOf(0); // 20

	private Integer note10 = Integer.valueOf(0); // 30

	private Integer note20 = Integer.valueOf(0); // 30

	private Integer note50 = Integer.valueOf(0); // 10

	private double moneyCount;

	public Integer getNote5() {
		return note5;
	}

	public void setNote5(Integer note5) {
		this.note5 = note5;
	}

	public Integer getNote10() {
		return note10;
	}

	public void setNote10(Integer note10) {
		this.note10 = note10;
	}

	public Integer getNote20() {
		return note20;
	}

	public void setNote20(Integer note20) {
		this.note20 = note20;
	}

	public Integer getNote50() {
		return note50;
	}

	public void setNote50(Integer note50) {
		this.note50 = note50;
	}

	public double getMoneyCount() {
		return note5.intValue() * 5 + note10.intValue() * 10 + note20.intValue() * 20 + note50.intValue() * 50;
	}

}
