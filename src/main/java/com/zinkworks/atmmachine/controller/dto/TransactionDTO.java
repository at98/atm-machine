package com.zinkworks.atmmachine.controller.dto;

/**
 * 
 * @author Arun.Singh
 *
 */
public class TransactionDTO {

	private DispensedCashDTO dispensedCashDto;
	private AccountBalanceDTO accountBalanceDto;

	public DispensedCashDTO getDispensedCashDto() {
		return dispensedCashDto;
	}

	public void setDispensedCashDto(DispensedCashDTO dispensedCashDto) {
		this.dispensedCashDto = dispensedCashDto;
	}

	public AccountBalanceDTO getAccountBalanceDto() {
		return accountBalanceDto;
	}

	public void setAccountBalanceDto(AccountBalanceDTO accountBalanceDto) {
		this.accountBalanceDto = accountBalanceDto;
	}

}
