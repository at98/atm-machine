package com.zinkworks.atmmachine.controller.dto;

/**
 * 
 * @author Arun.Singh
 *
 */

public class AccountBalanceDTO {

	private double balance;

	private double overDraftBalance;

	private double maximumWithdraw;

	public AccountBalanceDTO(double balance, double overDraftBalance, double maximumWithdraw) {

		this.balance = balance;
		this.overDraftBalance = overDraftBalance;
		this.maximumWithdraw = maximumWithdraw;
	}

	public AccountBalanceDTO() {
	}

	public double getRegularBalance() {
		return balance;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getOverDraftBalance() {
		return overDraftBalance;
	}

	public void setOverDraftBalance(double overDraftBalance) {
		this.overDraftBalance = overDraftBalance;
	}

	public double getMaximumWithdraw() {
		return maximumWithdraw;
	}

	public void setMaximumWithdraw(double maximumWithdraw) {
		this.maximumWithdraw = maximumWithdraw;
	}

}
