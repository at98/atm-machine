package com.zinkworks.atmmachine.controller.dto;

/**
 * 
 * @author Arun.Singh
 *
 */

public class AccountDebitDTO {

	private long accountId;
	private int pin;
	private double amount;

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

}