package com.zinkworks.atmmachine.exception;

import org.springframework.http.HttpStatus;

public class ATMApiError {

	private HttpStatus status = null;

	private String message = "";

	public ATMApiError(HttpStatus status, String message) {
		this.message = message;
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
}
