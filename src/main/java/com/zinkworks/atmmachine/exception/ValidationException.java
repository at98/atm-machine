package com.zinkworks.atmmachine.exception;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author Arun.Singh
 *
 */
public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = -5034492011577943064L;
	private HttpStatus status;

	public ValidationException(final HttpStatus status, final String message) {
		super(message);
		this.setStatus(status);
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
