package com.zinkworks.atmmachine.enums;

/**
 * 
 * @author Arun.Singh
 *
 */
public enum CurrencyEnum {

	FIFTY(50),

	TWENTY(20),

	TEN(10),

	FIVE(5);

	private final Integer value;

	private CurrencyEnum(final Integer value) {
		this.value = value;
	}

	public Integer value() {
		return value;
	}

	public static CurrencyEnum getMinimum() {
		return FIVE;
	}

}
