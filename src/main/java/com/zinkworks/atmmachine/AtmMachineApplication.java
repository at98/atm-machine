package com.zinkworks.atmmachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.zinkworks.atmmachine.notes.FiftyNoteDispenser;
import com.zinkworks.atmmachine.notes.FiveNoteDispenser;
import com.zinkworks.atmmachine.notes.NoteDispenser;
import com.zinkworks.atmmachine.notes.TenNoteDispenser;
import com.zinkworks.atmmachine.notes.TwentyNoteDispenser;

@SpringBootApplication
public class AtmMachineApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtmMachineApplication.class, args);
	}

	@Bean
	public NoteDispenser currencyDispenser() {
		final NoteDispenser fiftyCurrencyDispenser = new FiftyNoteDispenser();
		final NoteDispenser twentyCurrencyDispenser = new TwentyNoteDispenser();
		final NoteDispenser tenCurrencyDispenser = new TenNoteDispenser();
		final NoteDispenser fiveCurrencyDispenser = new FiveNoteDispenser();

		fiftyCurrencyDispenser.nextDispenser(twentyCurrencyDispenser);
		twentyCurrencyDispenser.nextDispenser(tenCurrencyDispenser);
		tenCurrencyDispenser.nextDispenser(fiveCurrencyDispenser);

		return fiftyCurrencyDispenser;
	}

}
