package com.zinkworks.atmmachine.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Arun.Singh
 *
 */
@Entity
@Table(name = "atm")
public final class ATM {

	@Id
	public final Long id = 1L;

	private Integer note5; // 20

	private Integer note10; // 30

	private Integer note20; // 30

	private Integer note50; // 10

	public ATM() {

	}

	public ATM(int note5, int note10, int note20, int note50) {
		this.note5 = note5;
		this.note10 = note10;
		this.note20 = note20;
		this.note50 = note50;
	}

	public Integer getNote5() {
		return note5;
	}

	public void setNote5(Integer note5) {
		this.note5 = note5;
	}

	public Integer getNote10() {
		return note10;
	}

	public void setNote10(Integer note10) {
		this.note10 = note10;
	}

	public Integer getNote20() {
		return note20;
	}

	public void setNote20(Integer note20) {
		this.note20 = note20;
	}

	public Integer getNote50() {
		return note50;
	}

	public void setNote50(Integer note50) {
		this.note50 = note50;
	}

	public Long getId() {
		return id;
	}

}
