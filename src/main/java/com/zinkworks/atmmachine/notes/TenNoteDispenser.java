package com.zinkworks.atmmachine.notes;

import com.zinkworks.atmmachine.entity.ATM;
import com.zinkworks.atmmachine.enums.CurrencyEnum;

public class TenNoteDispenser implements NoteDispenser {

	private NoteDispenser nextDispenser;

	@Override
	public DispenserResult dispense(final ATM atmDetails, final DispenserResult dispenserResult) {
		final int noOf10NotesInATM = atmDetails.getNote10();
		int dispensedAmountLeft = dispenserResult.getAtmBalance();
		if (dispensedAmountLeft >= CurrencyEnum.TEN.value() && noOf10NotesInATM >= 0) {
			final int withdrawNumber = dispensedAmountLeft / CurrencyEnum.TEN.value();
			if (noOf10NotesInATM >= withdrawNumber) {
				dispensedAmountLeft = dispensedAmountLeft % CurrencyEnum.TEN.value();
				atmDetails.setNote10(noOf10NotesInATM - withdrawNumber);
				dispenserResult.getDispensedCashDTO().setNote10(withdrawNumber);
			} else {
				dispensedAmountLeft = dispensedAmountLeft - noOf10NotesInATM * CurrencyEnum.TEN.value();
				dispenserResult.getDispensedCashDTO().setNote10(noOf10NotesInATM);
				atmDetails.setNote10(0);
			}
			dispenserResult.setAtmBalance(dispensedAmountLeft);
		}

		if (dispensedAmountLeft > 0 && nextDispenser != null) {
			return nextDispenser.dispense(atmDetails, dispenserResult);
		}

		return dispenserResult;
	}

	@Override
	public void nextDispenser(final NoteDispenser nextDispenser) {
		this.nextDispenser = nextDispenser;
	}

}
