package com.zinkworks.atmmachine.notes;

public class WithdrawalRequest {

	private long accountId;

	private int pin;

	private int amount;

	public WithdrawalRequest(long accountId, int pin, int amount) {
		this.accountId = accountId;
		this.pin = pin;
		this.amount = amount;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
