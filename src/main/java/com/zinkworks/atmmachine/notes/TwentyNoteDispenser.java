package com.zinkworks.atmmachine.notes;

import com.zinkworks.atmmachine.entity.ATM;
import com.zinkworks.atmmachine.enums.CurrencyEnum;

/**
 * 
 * @author Arun.Singh
 *
 */
public class TwentyNoteDispenser implements NoteDispenser {

	private NoteDispenser nextDispenser;

	@Override
	public DispenserResult dispense(final ATM atmDetails, final DispenserResult dispenserResult) {
		final int noOf20NotesInATM = atmDetails.getNote20();
		int dispensedAmountLeft = dispenserResult.getAtmBalance();
		if (dispensedAmountLeft >= CurrencyEnum.TWENTY.value() && noOf20NotesInATM >= 0) {
			final int withdrawNumber = dispensedAmountLeft / CurrencyEnum.TWENTY.value();
			if (noOf20NotesInATM >= withdrawNumber) {
				dispensedAmountLeft = dispensedAmountLeft % CurrencyEnum.TWENTY.value();
				atmDetails.setNote20(noOf20NotesInATM - withdrawNumber);
				dispenserResult.getDispensedCashDTO().setNote20(withdrawNumber);
			} else {
				dispensedAmountLeft = dispensedAmountLeft - noOf20NotesInATM * CurrencyEnum.TWENTY.value();
				dispenserResult.getDispensedCashDTO().setNote20(noOf20NotesInATM);
				atmDetails.setNote20(0);
			}
			dispenserResult.setAtmBalance(dispensedAmountLeft);
		}

		if (dispensedAmountLeft > 0 && nextDispenser != null) {
			return nextDispenser.dispense(atmDetails, dispenserResult);
		}

		return dispenserResult;
	}

	@Override
	public void nextDispenser(final NoteDispenser nextDispenser) {
		this.nextDispenser = nextDispenser;
	}

}
