package com.zinkworks.atmmachine.notes;

import com.zinkworks.atmmachine.entity.ATM;

/**
 * 
 * @author Arun.Singh
 *
 */
public interface NoteDispenser {

	void nextDispenser(NoteDispenser nextDispenser);

	DispenserResult dispense(final ATM atmDetails, final DispenserResult dispenserResult);

}
