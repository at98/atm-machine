package com.zinkworks.atmmachine.notes;

import com.zinkworks.atmmachine.entity.ATM;
import com.zinkworks.atmmachine.enums.CurrencyEnum;

/**
 * 
 * @author Arun.Singh
 *
 */
public class FiveNoteDispenser implements NoteDispenser {

	private NoteDispenser nextDispenser;

	@Override
	public DispenserResult dispense(final ATM atmDetails, final DispenserResult dispenserResult) {
		final int noOf5NotesInATM = atmDetails.getNote5();
		int dispensedAmountLeft = dispenserResult.getAtmBalance();
		if (dispensedAmountLeft >= CurrencyEnum.FIVE.value() && noOf5NotesInATM >= 0) {
			final int withdrawNumber = dispensedAmountLeft / CurrencyEnum.FIVE.value();
			if (noOf5NotesInATM >= withdrawNumber) {
				dispensedAmountLeft = dispensedAmountLeft % CurrencyEnum.FIVE.value();
				atmDetails.setNote5(noOf5NotesInATM - withdrawNumber);
				dispenserResult.getDispensedCashDTO().setNote5(withdrawNumber);
			} else {
				dispensedAmountLeft = dispensedAmountLeft - noOf5NotesInATM * CurrencyEnum.FIVE.value();
				dispenserResult.getDispensedCashDTO().setNote5(noOf5NotesInATM);
				atmDetails.setNote5(0);
			}
			dispenserResult.setAtmBalance(dispensedAmountLeft);
		}

		if (dispensedAmountLeft > 0 && nextDispenser != null) {
			return nextDispenser.dispense(atmDetails, dispenserResult);
		}

		return dispenserResult;
	}

	@Override
	public void nextDispenser(final NoteDispenser nextDispenser) {
		this.nextDispenser = nextDispenser;
	}

}
