package com.zinkworks.atmmachine.notes;

import com.zinkworks.atmmachine.controller.dto.DispensedCashDTO;

/**
 * 
 * @author Arun.Singh
 *
 */
public class DispenserResult {

	private DispensedCashDTO dispensedCashDTO;

	private int atmBalance;

	public DispenserResult(DispensedCashDTO dispensedCashDTO, int amount) {
		this.setAtmBalance(amount);
		this.setDispensedCashDTO(dispensedCashDTO);
	}

	public DispensedCashDTO getDispensedCashDTO() {
		return dispensedCashDTO;
	}

	public void setDispensedCashDTO(DispensedCashDTO dispensedCashDTO) {
		this.dispensedCashDTO = dispensedCashDTO;
	}

	public int getAtmBalance() {
		return atmBalance;
	}

	public void setAtmBalance(int atmBalance) {
		this.atmBalance = atmBalance;
	}

}
