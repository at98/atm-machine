package com.zinkworks.atmmachine.notes;

import com.zinkworks.atmmachine.entity.ATM;
import com.zinkworks.atmmachine.enums.CurrencyEnum;

/**
 * 
 * @author Arun.Singh
 *
 */
public class FiftyNoteDispenser implements NoteDispenser {

	private NoteDispenser nextDispenser;

	@Override
	public DispenserResult dispense(final ATM atmDetails, final DispenserResult dispenserResult) {
		final int noOf50NotesInATM = atmDetails.getNote50();
		int dispensedAmountLeft = dispenserResult.getAtmBalance();
		if (dispensedAmountLeft >= CurrencyEnum.FIFTY.value() && noOf50NotesInATM >= 0) {
			final int withdrawNumber = dispensedAmountLeft / CurrencyEnum.FIFTY.value();
			if (noOf50NotesInATM >= withdrawNumber) {
				dispensedAmountLeft = dispensedAmountLeft % CurrencyEnum.FIFTY.value();
				atmDetails.setNote50(noOf50NotesInATM - withdrawNumber);
				dispenserResult.getDispensedCashDTO().setNote50(withdrawNumber);
			} else {
				dispensedAmountLeft = dispensedAmountLeft - noOf50NotesInATM * CurrencyEnum.FIFTY.value();
				dispenserResult.getDispensedCashDTO().setNote50(noOf50NotesInATM);
				atmDetails.setNote50(0);
			}
			dispenserResult.setAtmBalance(dispensedAmountLeft);
		}

		if (dispensedAmountLeft > 0 && nextDispenser != null) {
			return nextDispenser.dispense(atmDetails, dispenserResult);
		}

		return dispenserResult;
	}

	@Override
	public void nextDispenser(final NoteDispenser nextDispenser) {
		this.nextDispenser = nextDispenser;
	}

}
